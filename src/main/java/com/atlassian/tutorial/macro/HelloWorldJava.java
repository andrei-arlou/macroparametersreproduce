package com.atlassian.tutorial.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;

import java.util.Map;

public class HelloWorldJava implements Macro {

    @Override
    public String execute(Map<String, String> map, String s, ConversionContext conversionContext) {
        return "<h1>Hello World</h1>";
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
